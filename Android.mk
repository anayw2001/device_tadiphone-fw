LOCAL_PATH:= $(call my-dir)

ifneq ($(filter TP1803 mini5g,$(TARGET_DEVICE)),)

# artifacts from caf build
ifeq (TP1803, $(TARGET_DEVICE))
$(call add-radio-file,dtbo.img)
endif
$(call add-radio-file,vendor.img)

# qualcomm test images
$(call add-radio-file,fw/tz.mbn)
$(call add-radio-file,fw/km4.mbn)

# from NX627J A11
$(call add-radio-file,fw/aop.mbn)
$(call add-radio-file,fw/BTFM.bin)
$(call add-radio-file,fw/cmnlib.mbn)
$(call add-radio-file,fw/cmnlib64.mbn)
$(call add-radio-file,fw/dspso.bin)
$(call add-radio-file,fw/hyp.mbn)
$(call add-radio-file,fw/modem.img)
$(call add-radio-file,fw/qupv3fw.elf)
$(call add-radio-file,fw/storsec.mbn)
$(call add-radio-file,fw/uefi_sec.mbn)

endif
