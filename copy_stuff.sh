if [ ! -z $OUT ]
then
	rm dtbo.img vendor.img prebuilt.dtb kernel
	cp $OUT/dtbo.img $OUT/vendor.img .
	python3 ../../system/tools/mkbootimg/unpack_bootimg.py --out ./out --boot_img $OUT/boot.img >> /dev/null
	cp out/dtb prebuilt.dtb
	cp out/kernel kernel
	rm -rf out/
	simg2img vendor.img vendor.rimg
	rm vendor.img
	mv vendor.rimg vendor.img
	cp $OUT/boot.img ./cafboot.img
	echo "All done!"
else
	echo "\$OUT is not set!"
fi
